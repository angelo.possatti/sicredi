<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','cpf','rg','birth_date','address','post_address'];
    protected $dates = ['deleted_at'];
}
