<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactMean extends Model
{
	use SoftDeletes;

    protected $fillable = ['title'];
    protected $dates = ['deleted_at'];

    /**
     * Busca os campos da tabela
     */
    public static function getFields() {
    	return ContactMean::all();
    }

     /**
     * Busca apenas campos com o id parametrizado
     * @param int $id identificador da forma de contato
     */
    public static function getField($id) {
		return ContactMean::where('id','=',$id)->get();
    } 
}
