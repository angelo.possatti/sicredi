<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Client;
use App\ContactMean;
use App\ClientContact;
use App\ActionAudit;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all('id','name','cpf','rg','birth_date','address');
        
        $clients = json_decode($clients,true);  

        foreach ($clients as $key => $client) {
            $clients[$key]['age'] = $this->getClientAge($client['birth_date']);
            $clients[$key]['birth_date'] = date('d/m/Y', strtotime($clients[$key]['birth_date']));
        }

        $clients = json_encode($clients);

        // formas de contato
        $contactMeans = ContactMean::getFields();

        return view('clients.index', compact('clients','contactMeans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validation = $this->toValidate($data);

        if ( $validation->fails() ) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $client = Client::create($data);

        // Auditoria de ação
        $this->setAudit('Inserção', $client->id);

        // Se existir formas de contato na persistência, tratar
        if ( isset($data['contact_means']) && !empty($data['contact_means']) ) {
            $this->contactMeans($client->id,$data['contact_means']);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $client = Client::find($id);
        $client['contact_means'] = $this->getContactMeans($id);
        $client['contact_means_title'] = $this->getContactMeansTitle($client['contact_means']);

        if ( count($client['contact_means_title']) < 1 ) {
            $client['contact_means_title'] = 'Nenhuma forma vinculada';
        }

        return $client;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validation = $this->toValidate($data,$id);

        if ( $validation->fails() ) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        Client::find($id)->update($data);

        // Auditoria de ação
        $this->setAudit('Alteração', $id);

        // Se existir formas de contato na persistência, tratar
        if ( isset($data['contact_means']) && !empty($data['contact_means']) ) {
            $this->contactMeans($id,$data['contact_means']);
        }
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Client::find($id)->delete();

        // Auditoria de ação
        $this->setAudit('Remoção', $id);
        return redirect()->back();
    }

    /**
     * Busca informações atuais de total de clientes e idades
     * 
     * @return \Illuminate\Http\Response
     */
    public function getMonitorData() {
        $clients = Client::all('birth_date');
        $clients = json_decode($clients,true); 

        $totalAges = 0; 

        foreach ($clients as $key => $client) {
            $totalAges += intval( $this->getClientAge($client['birth_date'] ));
        }

        $totalClients = Client::count();

        $data = [ 'totalClients' => $totalClients, 'totalAges' => $totalAges];
        return response()->json($data);
    }

     /**
     * Action da tela de monitor
     *
     * @param  View da tela de monitor
     */
    public function monitor() {
        return view('clients.monitor');
    }

    /**
     * Persiste as formas de contato.
     *
     * @param  int  $id identificador do cliente
     * @param  Array  $contact_means identificadores das formas de contato
     */
    private function contactMeans($id,$contact_means) {
        foreach ($contact_means as $contact_mean) {
            ClientContact::create(['client_id'=>$id, 'contact_mean_id' => $contact_mean]);
        }
    }

    /**
     * Busca id das formas de contato.
     *
     * @param  int  $id identificador do cliente
     * @param  Array  $contact_means identificadores das formas de contato
     */
    private function getContactMeans($id) {
        $resp = ClientContact::where('client_id','=',$id)->get();
        $contactMeans = [];

        for ($i=0; $i < count($resp) ; $i++) { 
            $contactMeans[$i] = $resp[$i]['contact_mean_id'];
        }

        return $contactMeans;
    }

    /**
     * Busca título das formas de contato.
     *
     */
    private function getContactMeansTitle($contactMeansIds) {
        $titles = [];

        foreach ($contactMeansIds as $key => $contactMeansId) {
            $contactMean = ContactMean::getField($contactMeansId); 
            $contactMean = json_decode($contactMean,true);

            foreach ($contactMean as $key => $type) {
                if ($key == 'title') {
                    $titles[] = $contactMean[$key]['title'];
                }
            }
        }

        return $titles;
    }
    /**
     * Valida dados que serão persistidos.
     *
     * @param  App\Client  $client
     * @param  Boolean  $id
     * @return Mixed $validation
     */
    private function toValidate($client,$id = false) {
        // Quando for um registro existente é necessário ignorar a validação
        if ($id) {
            $validation = \Validator::make($client,[
                'name' => 'required',
                'cpf' => ['required',Rule::unique('clients')->ignore($id)],
                'rg' => ['required',Rule::unique('clients')->ignore($id)],
                'birth_date' => 'required',
                'address' => 'required',
                'post_address' => 'required',
            ]);

            return  $validation;
        }

        $validation = \Validator::make($client,[
            'name' => 'required',
            'cpf' => ['required',Rule::unique('clients')],
            'rg' => ['required',Rule::unique('clients')],
            'birth_date' => 'required',
            'address' => 'required',
            'post_address' => 'required',
        ]);

        return  $validation;
    }

    /**
     * Calcula idade do cliente
     *
     * @param  String $birth_date data de nascimento
     * @return String Idade do cliente
     */
    private function getClientAge($birth_date) {
        return (date('Y') - date('Y',strtotime($birth_date)));
    }

    /**
     * Persiste auditoria cliente
     *
     * @param  String $action ação para log
     * @return Int registryId identificador do cliente
     */
    private function setAudit($action,$registryId) {
        $user = Auth::user();

        // Auditoria
        $audit = [
            'user_id' => $user->id,
            'user_login' => $user->name . ' - ' . $user->email,
            'date' => date('Y-m-d H:i:s'),
            'local' => 'Clientes',
            'action' => $action,
            'registry_id' => $registryId,
        ];

        ActionAudit::create($audit);
    }
}
