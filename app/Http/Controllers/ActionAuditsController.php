<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ActionAudit;
use Barryvdh\DomPDF\Facade as PDF;

class ActionAuditsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = ActionAudit::orderBy('date', 'desc')->get(['user_id','user_login','date','local','action','registry_id']);

        foreach ($data as $key => $audit) {
            $data[$key]['date'] = date('d/m/Y H:i:s', strtotime($data[$key]['date']));
        }

        return view('action_audits.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Gerar Stream do relatório de auditoria em PDF
     *
     * @return \Illuminate\Http\Response
     */
    public function report(){
        $data['date'] = date('d/m/Y H:i');

        $data['data'] = ActionAudit::orderBy('date', 'desc')->get(['user_id','user_login','date','local','action','registry_id']);

        foreach ($data['data'] as $key => $audit) {
            $data['data'][$key]['date'] = date('d/m/Y H:i:s', strtotime($data['data'][$key]['date']));
        }

        return PDF::loadView('action_audits.report', $data)
         ->stream();
    }

}
