<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactMean;

class ContactMeansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = json_encode(ContactMean::all('id','title'));

        return view('contact_means.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validation = $this->toValidate($data);

        if ( $validation->fails() ) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        ContactMean::create($data);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ContactMean::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validation = $this->toValidate($data);

        if ( $validation->fails() ) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        ContactMean::find($id)->update($data);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ContactMean::find($id)->delete();
        return redirect()->back();
    }

    /**
     * Função privada para validar dados que serão persistidos.
     *
     * @param  App\ContactMean  $contactMean
     * @return Mixed $validation
     */
    private function toValidate($contactMean) {
         $validation = \Validator::make($contactMean,[
            'title' => 'required',
        ]);

        return  $validation;
    }
}
