@extends('layouts.app')

@section('content')
    <page-component>
        @if($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                @if($errors->all())   
                    @foreach ($errors->all() as $value)
                        <li>{{ $value }}</li>
                    @endforeach
                @endif
            </div>
        @endif

        <panel-component headline="Clientes">
        	<table-component 
        		v-bind:fields="['ID','Nome','CPF','RG','Data Nascimento', 'Endereço', 'Idade']"
        		v-bind:items="{{ $clients }}"
                read = "/clientes/"
                update = "/clientes/"
                remove = "/clientes/"
        		token="{{ csrf_token() }}">
        	</table-component>
        </panel-component>
    </page-component>

    {{-- Modal para adicionar novo cliente --}}
    <modal-component id="toCreate" title="Novo">
        <form-component 
            id="form-create"
            action="{{ route('clientes.store') }}" 
            method="POST"
            enctype=""
            token="{{ csrf_token() }}">

            {{-- Nome do Cliente --}}
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" required="true" class="form-control" id="name" name="name" placeholder="Ex: João Silva" value="{{ old('name') }}">
            </div>

            {{-- CPF do Cliente --}}
            <div class="form-group">
                <label for="cpf">CPF</label>
                <input type="text" required="true" onkeyup="onlyNumber(this);" maxlength="11" class="form-control" id="cpf" name="cpf" placeholder="Ex: 01234567890" value="{{ old('cpf') }}">
            </div>

            {{-- RG do Cliente --}}
            <div class="form-group">
                <label for="rg">RG</label>
                <input type="text" required="true" onkeyup="onlyNumber(this);"maxlength="12" class="form-control" id="rg" name="rg" placeholder="Ex: 01234567890" value="{{ old('rg') }}">
            </div>

            {{-- Data Nascimento do Cliente --}}
            <div class="form-group">
                <label for="birth_date">Data Nascimento</label>
                <input type="date" required="true" class="form-control" id="birth_date" name="birth_date" placeholder="Ex: 20/07/1980" value="{{ old('birth_date') }}">
            </div>

            {{-- Endereço do Cliente --}}
            <div class="form-group">
                <label for="address">Endereço Residencial</label>
                <input type="text" required="true" class="form-control" id="address" name="address" placeholder="Ex: Avenida Hélvio Basso, 1666" value="{{ old('address') }}">
            </div>

            {{-- Endereço correspondência do Cliente --}}
            <div class="form-group">
                <label for="post_address">Endereço de Correspondência</label>
                <input type="text" required="true" class="form-control" id="post_address" name="post_address" placeholder="Ex: Avenida Hélvio Basso, 1666" value="{{ old('post_address') }}">
            </div>

            {{-- Formas de Contato --}}
            <checkbox-component v-bind:items="{{ $contactMeans }}"></checkbox-component>
        </form-component>

        {{-- Botões --}}
        <span slot="buttons">
            <button form="form-create" class="btn btn-info">Salvar</button>
        </span>
    </modal-component>

    {{-- Modal para editar cliente --}}
    <modal-component id="toUpdate" title="Editar">
        <form-component 
            id="form-edit"
            v-bind:action="'/clientes/' + $store.state.item.id" 
            method="PUT"
            enctype="multipart/form-data" 
            token="{{ csrf_token() }}">

            {{-- Nome do Cliente --}}
            <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" required="true" class="form-control" id="name" name="name" v-model="$store.state.item.name">
            </div>

            {{-- CPF do Cliente --}}
            <div class="form-group">
                <label for="cpf">CPF</label>
                <input type="text" required="true" onkeyup="onlyNumber(this);" maxlength="11" class="form-control" id="cpf" name="cpf" v-model="$store.state.item.cpf">
            </div>

            {{-- RG do Cliente --}}
            <div class="form-group">
                <label for="rg">RG</label>
                <input type="text" required="true" onkeyup="onlyNumber(this);" maxlength="12" class="form-control" id="rg" name="rg" placeholder="01234567890" v-model="$store.state.item.rg">
            </div>

            {{-- Data Nascimento do Cliente --}}
            <div class="form-group">
                <label for="birth_date">Data Nascimento</label>
                <input type="date" required="true" class="form-control" id="birth_date" name="birth_date" placeholder="20/07/1980" v-model="$store.state.item.birth_date">
            </div>

            {{-- Endereço do Cliente --}}
            <div class="form-group">
                <label for="address">Endereço Residencial</label>
                <input type="text" required="true" class="form-control" id="address" name="address" placeholder="Avenida Hélvio Basso, 367" v-model="$store.state.item.address">
            </div>

            {{-- Endereço correspondência do Cliente --}}
            <div class="form-group">
                <label for="post_address">Endereço de Correspondência</label>
                <input type="text" required="true" class="form-control" id="post_address" name="post_address" placeholder="Avenida Hélvio Basso, 367" v-model="$store.state.item.post_address">
            </div>

            {{-- Formas de Contato --}}
            <checkbox-component v-bind:items="{{ $contactMeans }}"></checkbox-component>

        </form-component>

        {{-- Botões --}}
        <span slot="buttons">
            <button form="form-edit" class="btn btn-info">Salvar</button>
        </span>
    </modal-component>

    {{-- Modal para visualizar detalhes do cliente --}}
    <modal-component id="toRead" title="Detalhes">
        {{-- Nome do Cliente --}}
        <div class="form-group">
            <label for="name">Nome: </label> 
            @{{ $store.state.item.name }}
        </div>

        {{-- Endereço Postal do Cliente --}}
        <div class="form-group">
            <label for="post_address">Endereço Postal: </label>
             @{{ $store.state.item.post_address }}
        </div>

        <div class="form-group">
            <label for="contact_means">Formas de Contato: </label>
             @{{ $store.state.item.contact_means_title }}
        </div>
    </modal-component>
@endsection

<style type="text/css">
    table {
        font-size: 14px;
    }
</style>

<script type="text/javascript">
    function onlyNumber(param) {
        var er = /[^0-9]/;
        er.lastIndex = 0;
        var field = param;
        if (er.test(field.value)) {
          field.value = "";
        }
    }
</script>