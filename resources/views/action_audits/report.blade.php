<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>Auditoria de Ações</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	    <style type="text/css">
	    	table {
	    		font-size: 11px;
	    	}
	    </style>

	</head>

	<h4 style="text-align: center;">Auditoria de Ações - {{ $date }} </h4>

	<body>
		<div>
		    <table class="table table-responsive table-striped table-hover">
		        <thead>
		            <tr>
		                <th>Usuário - ID</th>
		                <th>Usuário - Login</th>
		                <th>Data</th>
		                <th>Local</th>
		                <th>Ação</th>
		                <th>ID do Registro</th>
		            </tr>
		        </thead>
		        <tbody>
		            @foreach($data as $item)
			            <tr>
			                <td>{{$item->user_id}}</td>
			                <td>{{$item->user_login}}</td>
			                <td>{{$item->date}}</td>
			                <td>{{$item->local}}</td>
			                <td>{{$item->action}}</td>
			                <td>{{$item->registry_id}}</td>
			            </tr>
		            @endforeach
		        </tbody>
		    </table>
		</div>
	</body>
</html>